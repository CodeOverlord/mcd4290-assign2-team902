// Declare a reference for setValue to display it on the page
let setValueRef = document.getElementById('setValue')
setValueRef.innerHTML = maxDistancePosts + ' m';

// Function for setting distance between each post in meter
function setPostDistance() {
    //Prompt the user to enter a value and update that value to the page
    let value = prompt('Enter a value for maximum post distance in meters');
    if (value === '' || value === null || isNaN(value)) {
        displayMessage('Distance between posts reset to 4: Please enter a number...', 3000)
        resetPostDistance();
    } else {
        localStorage.setItem('DISTANCE', value);
        setValueRef.innerHTML = value + ' m';
    }
}

// Function to reset distance between each post to 4 meter
function resetPostDistance() {
    localStorage.removeItem('DISTANCE');
    setValueRef.innerHTML = 4 + ' m';
}