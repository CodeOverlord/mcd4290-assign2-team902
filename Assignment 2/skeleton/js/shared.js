// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.fencingApp";

// An instance of geodesic class that allow access to the GeographicLib library
let geod = GeographicLib.Geodesic.WGS84;

// Set distance between the posts
let maxDistancePosts = 4;

if ('DISTANCE' in localStorage) {
    maxDistancePosts = JSON.parse(localStorage.getItem('DISTANCE'));
}

// Class for creating new Region instances
class Region {
    constructor(locationArr) {
        //Privates Attribute:
        this._cornerLocations = locationArr;
        this._date = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        this._time = new Date().getHours() + ':' + (new Date().getMinutes() + 1) + ':' + new Date().getSeconds();
    }
    
    //Public methods:
    get cornerLocations() {
        return this._cornerLocations;
    }
    
    get date() {
        return this._date;
    }
    
    set date(newDate) {
        this._date = newDate;
    }
    
    get time() {
        return this._time;
    }
    
    set time(newTime) {
        this._time = newTime;
    }
    
    area() {
        return this._getAreaAndPerimeter()[0];
    }
    
    perimeter() {
        return this._getAreaAndPerimeter()[1];
    }
    
    // Method to get the array of fence posts' locations
    boundaryFencePosts() {
        let boundaryFencePosts = [];
        for (let i = 0; i < this._cornerLocations.length; i++) {
            boundaryFencePosts.push(this._cornerLocations[i]);
        }
        for (let i = 0; i < this._cornerLocations.length - 1; i++) {
            let distAndDir = geod.Inverse(this._cornerLocations[i][1], this._cornerLocations[i][0], this._cornerLocations[i+1][1], this._cornerLocations[i+1][0]);
            for (let j = maxDistancePosts; j < distAndDir.s12; j += maxDistancePosts) {
                let postLocation = geod.Direct(this._cornerLocations[i][1], this._cornerLocations[i][0], distAndDir.azi1, j);
                boundaryFencePosts.push([postLocation.lon2, postLocation.lat2]);
            }
        }
        for (let i = 0; i < boundaryFencePosts.length; i++) {
            if (JSON.stringify(boundaryFencePosts[i]) === JSON.stringify(boundaryFencePosts[0])) {
                boundaryFencePosts.splice(i, 1);
            }
        }
        return boundaryFencePosts;
    }
    
    //Private methods:
    
    // Method to calculate area and perimeter and store these values in an array to allow easy access to these values
    _getAreaAndPerimeter() {
        /*
        let retreivedData = JSON.parse(localStorage.getItem("monash.mcd4290.fencingApp"));
        let coordinates = retreivedData._cornerLocations;
        var p = geod.Polygon(false);
        for (let i = 0; i < coordinates.length; i++) {
            for (let j = 0; j < coordinates[0]._cornerLocations.length; j++) {
                p.AddPoint(coordinates[i]._cornerLocations[j][0],coordinates[i]._cornerLocations[j][1]);
                p = p.Compute(false, true);
                this._areaAndPerimeter = "Perimeter/Area: " + p.perimeter.toFixed(3) + " m/ " + p.area.toFixed(3) + " m^2";
            }
        }
        */
        let regionToCalculate = geod.Polygon(false);
        for (let i = 0; i < this._cornerLocations.length; i++) {
            regionToCalculate.AddPoint(this._cornerLocations[i][1], this._cornerLocations[i][0]);
        }
        regionToCalculate = regionToCalculate.Compute(false,true);
        return [Math.abs(regionToCalculate.area).toFixed(2), regionToCalculate.perimeter.toFixed(2)];
    }
}

// Class for creating new RegionList instances
class RegionList {
    constructor() {
        //Privates Attribute:
        this._currentRegions = [];
        this._numberOfRegions = this._currentRegions.length;
    }
    
    //Public methods:
    get currentRegions() {
        return this._currentRegions;
    }
    
    set currentRegions(regionArr) {
        this._currentRegions = regionArr;
    }
    
    get numberOfRegions() {
        return this._numberOfRegions;
    }
    
    set numberOfRegions(numOfRegions) {
        this._numberOfRegions = numOfRegions;
    }
    
    addRegion(newRegion) {
        this._currentRegions.push(newRegion);
        this._numberOfRegions++;
    }
    
    removeRegion(regionToRemove) {
        for (let i = 0; i < this._numberOfRegions; i++) {
            if (JSON.stringify(this._currentRegions[i]) === JSON.stringify(regionToRemove)) {
                this._currentRegions.splice(i, 1);
            }
        }
        this._numberOfRegions--;
    }
}

// Function to create a new Region instance out of dummy object
function newRegionInstance(dummyObject) {
    region = new Region(dummyObject._cornerLocations);
    region.date = dummyObject._date;
    region.time = dummyObject._time;
    return region;
}

// Function to create a new RegionList instance out of dummy object
function newRegionListInstance(dummyObject) {
    regionList = new RegionList();
    regionList.currentRegions = dummyObject._currentRegions;
    regionList.numberOfRegions = dummyObject._numberOfRegions;
}

// An instance of RegionList class used for storing created regions
let regionList = new RegionList();
