// Code for the View Region page.

// Retrieve the region from local storage and create a new Region instance from dummy object
let retrievedRegion = JSON.parse(localStorage.getItem(APP_PREFIX + "-selectedRegion"));
let viewedRegion = newRegionInstance(retrievedRegion);
let fencePostsArr = [];

//Key to access mapbox API
mapboxgl.accessToken = 'pk.eyJ1IjoiY2h1dGlwb24iLCJhIjoiY2tpdmR6MTJkMTdhNTMybW1mN2FmeWxzZCJ9.dGjzI1XIopYuhY6p2UnQAQ';

let clayton = [145.1343136, -37.9110467];

// Settings for initial zoom, style, and center of the map
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 18,
    center: viewedRegion.cornerLocations[0]
});

// When the page is loaded, create a polygon with the locations of viewing region
map.on('load', function() {
    map.addSource('region', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'Polygon',
                'coordinates': [viewedRegion.cornerLocations]
            }
        }
    });
            
    map.addLayer({
        'id': 'region',
        'type': 'fill',
        'source': 'region',
        'layout': {},
        'paint': {
            'fill-color': '#088',
            'fill-opacity': 0.8
        }
    });
})

// Display the area and perimeter of the region on the page
document.getElementById('Area').innerHTML = 'Area: '.bold() + viewedRegion.area() + ' m' + '2'.sup();
document.getElementById('Perimeter').innerHTML = 'Perimeter: '.bold() + viewedRegion.perimeter() + ' m';

// Function to toggle posts
function togglePosts() {
    let fencePostsLocation = viewedRegion.boundaryFencePosts();
    if (fencePostsArr.length === 0) {
        for (let i = 0; i < fencePostsLocation.length; i++) {
            let fencePosts = new mapboxgl.Marker().setLngLat(fencePostsLocation[i]).addTo(map);
            fencePostsArr.push(fencePosts);
        }
        displayMessage(fencePostsLocation.length + ' posts required', 3000);
    } else {
        for (let i = 0; i < fencePostsArr.length; i++) {
            fencePostsArr[i].remove();
        }
        fencePostsArr = [];
    }
}

// Function to remove a viewing region from the list and local storage
function removeRegion() {
    if (confirm('Do you want to delete this region?')) {
        let retrievedRegionList = JSON.parse(localStorage.getItem(APP_PREFIX));
        newRegionListInstance(retrievedRegionList);
        regionList.removeRegion(viewedRegion);
        localStorage.setItem(APP_PREFIX, JSON.stringify(regionList));
        location.href = 'index.html';
    }
}