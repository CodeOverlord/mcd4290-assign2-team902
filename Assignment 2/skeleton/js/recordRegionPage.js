// Code for the Record Region page.

//Key to access mapbox API
mapboxgl.accessToken = 'pk.eyJ1IjoiY2h1dGlwb24iLCJhIjoiY2tpdmR6MTJkMTdhNTMybW1mN2FmeWxzZCJ9.dGjzI1XIopYuhY6p2UnQAQ';

let clayton = [145.1343136, -37.9110467];
         
// Settings for initial zoom, style, and center of the map
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 18,
    center: clayton
});

let savedMarker = []; // Array used to store added markers
let savedLocation = []; // Array used to store location [lng,lat] of added markers
let enabled = true; // Enable a set of code
let i = 0; // Index of savedMarker array

// Button references
let addCornerButtonRef = document.getElementById('add');
let resetButtonRef = document.getElementById('reset');
let saveRegionButtonRef = document.getElementById('saveRegion')

// Disable the buttons
addCornerButtonRef.disabled = true;
resetButtonRef.disabled = true;
saveRegionButtonRef.disabled = true;

// Function that will execute when a click action occurs on the map
map.on('click', function(e) {
    // Automatkcally remove previously added marker and its location from the arrays when a new marker is added
    if (enabled === true) {
        if (savedMarker.length >= 1) {
            savedMarker[i].remove();
            savedLocation.splice(savedLocation.length - 2,2);
            i++;
        }
    }
    
    // Add a new marker and save it as well as its location to the created arrays, and pan the map to the location of the marker
    let marker = new mapboxgl.Marker().setLngLat(e.lngLat).addTo(map);
    savedMarker.push(marker);
    savedLocation.push(Object.values(e.lngLat));
    map.panTo(e.lngLat);
    enabled = true;
    
    // Remove repeating [0] location from an array
    for (let n = 1; n < savedLocation.length; n++) {
        if (JSON.stringify(savedLocation[n]) === JSON.stringify(savedLocation[0])) {
            savedLocation.splice(n,1);
            }
    }
    
    // Push the first location again as the last location of an array to create a polygon
    savedLocation.push(savedLocation[0]);
    
    // Add a polygon to the map
    removeLayerWithId('region');
    map.addSource('region', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'Polygon',
                'coordinates': [savedLocation]
            }
        }
    });
            
    map.addLayer({
        'id': 'region',
        'type': 'fill',
        'source': 'region',
        'layout': {},
        'paint': {
            'fill-color': '#088',
            'fill-opacity': 0.8
        }
    });
    
    // Enable the buttons
    addCornerButtonRef.disabled = false;
    resetButtonRef.disabled = false;
    
    // Allow the user to save the region once there are 3 or more corners
    if (savedLocation.length >= 4) {
        saveRegionButtonRef.disabled = false;
    }
});

// Function to add a corner to location array
function addCorner() {
    // Prevent a set of code that will remove the marker and its location from executing
    enabled = false;
    i++;
    addCornerButtonRef.disabled = true;
}

// Function to reset the locations and remove the polygon
function reset () {
    // Remove all saved markers from the map
    for (let i = 0; i < savedMarker.length; i++) {
        savedMarker[i].remove();
    } 
    
    // Clear all saved locations and remove layer and source, and pan to clayton
    savedLocation = [];
    map.removeLayer('region');
    map.removeSource('region');
    map.panTo(clayton);
    addCornerButtonRef.disabled = true;
    resetButtonRef.disabled = true;
    saveRegionButtonRef.disabled = true;
}

// Function to save a region in a regionList instance and store that instance to the local storage
function saveRegion() {
    if (confirm("Do you want to save this region?")) {
        if (APP_PREFIX in localStorage) {
            let retrievedRegionList = JSON.parse(localStorage.getItem(APP_PREFIX));
            newRegionListInstance(retrievedRegionList);
        }
        regionList.addRegion(new Region(savedLocation));
        localStorage.setItem(APP_PREFIX, JSON.stringify(regionList));
        location.href = 'index.html';
    }
}

// Function that checks whether there is a map layer with id matching idToRemove.  If there is, it is removed. (Given in Week 5's practical)
function removeLayerWithId(idToRemove) {
    let hasPoly = map.getLayer(idToRemove);
    if (hasPoly !== undefined) {
        map.removeLayer(idToRemove);
        map.removeSource(idToRemove);
    }
}

