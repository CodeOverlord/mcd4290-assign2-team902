// Code for the main app page (Regions List).

// Function to view a selected region
function viewRegion(desiredRegion)
{   
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", JSON.stringify(desiredRegion));
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}

// Check if there is already a data store with a key APP_PREFIX in local storage. If there is, 
// retrieve it from the local storage and create a new RegionList instance with the same properties as those of dummy object.
if (APP_PREFIX in localStorage) {
    let retrievedRegionList = JSON.parse(localStorage.getItem(APP_PREFIX));
    newRegionListInstance(retrievedRegionList);
}

// Display saved regions on the main page
for (let i = 0; i < regionList.numberOfRegions; i++) {
    // Create a new Region instance with the same properties as those of dummy objects
    let clonedRegion = newRegionInstance(regionList.currentRegions[i]);
    
    // <ul> node reference 
    var ul = document.getElementsByTagName("ul")[0];
    
    // Create a <li> node and set its attributes
    let liTempNode = document.createElement('LI');
    liTempNode.setAttribute('class', 'mdl-list__item mdl-list__item--two-line');
    liTempNode.onclick = function() { viewRegion(clonedRegion) };
    
    // Create an outter <span> node and set its attributes
    let outSpanTempNode = document.createElement('SPAN');
    outSpanTempNode.setAttribute('class', 'mdl-list__item-primary-content');
    
    // Create an inner <span> node and set its attributes
    let inSpan1TempNode = document.createElement('SPAN');
    inSpan1TempNode.innerHTML = clonedRegion.date + ' @ ' + clonedRegion.time;
    
    // Create an inner <span> node and set its attributes
    let inSpan2TempNode = document.createElement('SPAN');
    inSpan2TempNode.setAttribute('class', 'mdl-list__item-sub-title');
    inSpan2TempNode.innerHTML = 'Saved region';
    
    // Store created nodes inside each other
    outSpanTempNode.appendChild(inSpan1TempNode);
    outSpanTempNode.appendChild(inSpan2TempNode);
    liTempNode.appendChild(outSpanTempNode);
    ul.appendChild(liTempNode);
}

